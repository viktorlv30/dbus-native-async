'use strict';

/**
 * Helper module 
 */

/**
 * Проверяет, является ли значение экземпляром булевого типа
 * @param {*} value
 * @returns {boolean}
 */
function isBoolean(value) {
	return typeof value === 'boolean';
}

/**
 * Проверяет, является ли значение экземпляром строкового типа
 * @param {*} value
 * @returns {boolean}
 */
function isString(value) {
	return typeof value === 'string';
}

/**
 * Проверяет, является ли значение конечным числом
 * @param {*} value
 * @returns {boolean}
 */
function isNumber(value) {
	return typeof value === 'number' && Number.isFinite(value);
}

/**
 * Проверяет, является ли значение целым числом
 * @param {*} value
 * @returns {boolean}
 */
function isInteger(value) {
	return typeof value === 'number' && Number.isInteger(value);
}

/**
 * Проверяет, является ли значение положительным числом
 * @param {*} value
 * @returns {boolean}
 */
function isPositiveNumber(value) {
	return isNumber(value) && value > 0;
}

/**
 * Проверяет, является ли значение положительным целым числом
 * @param {*} value
 * @returns {boolean}
 */
function isPositiveInteger(value) {
	return isInteger(value) && value > 0;
}

/**
 * Проверяет, является ли значение функцией
 * @param {*} value
 * @returns {boolean}
 */
function isFunction(value) {
	return value instanceof Function;
}

/**
 * Проверяет, является ли значение массивом
 * @param {*} value
 * @returns {boolean}
 */
function isArray(value) {
	return value instanceof Array;
}

/**
 * Проверяет, является ли значение обьектом (заданого типа BaseClass)
 * @param {*} value
 * @param {Function} [BaseClass]
 * @returns {boolean}
 */
function isObject(value, BaseClass) {
	if (isFunction(BaseClass) && BaseClass !== Object)
		return value instanceof BaseClass;
	else
		return (value instanceof Object) && !isBoolean(value) && !isFunction(value) && !isString(value) && !isNumber(value);
}

/**
 * Определяет имя типа значения
 * @param value
 * @returns {string}
 */
function getClassName(value) {
	if (value === undefined)
		return 'undefined';
	if (value === null)
		return 'null';
	return value.constructor.name;
}

/**
 * Дополняет обьект указанными свойствами
 * @param {Object|Array} object - обьект, который нужно дополнить
 * @param {Object|Array} properties - набор свойств
 * @returns {Object|Array} - дополненный обьект
 * @throws {ArgumentsError} - некорректный набор аргументов
 */
function extend(object, properties) {
	if (!(isObject(object) || isArray(object)))
		throw new ArgumentsError('util.extend(object, properties): Instance of Object or Array expected as object');
	if (!(isObject(properties) || isArray(properties)))
		throw new ArgumentsError('util.extend(object, properties): Instance of Object or Array expected as properties');
	if (!(isObject(object, properties.constructor)))
		throw new ArgumentsError('util.extend(object, properties): object must be inherited from properties');

	for (var prop in properties) {
		if (properties.hasOwnProperty(prop)) {
			object[prop] = properties[prop];
		}
	}
	return object;
}

/**
 * Добавляет объекту заданный метод
 * @param {Object} object - объект, которому нужно добавить метод
 * @param {Function} method - метод, который нужно добавить
 * @param {*} [method_name] - имя метода, если указана строка
 * @returns {Object}
 */
function defineMethod(object, method, method_name) {
	if (!isObject(object))
		throw new TypeError('defineMethod(object, method, method_name): Instance of Object expected as object');
	if (!isFunction(method))
		throw new TypeError('defineMethod(object, method, method_name): Instance of Function expected as method');
	if (!(isString(method_name) || isString(method.name)))
		throw new TypeError('defineMethod(object, method, method_name): Instance of String expected as method_name or method.name');
	if (!((isString(method_name) && method_name !== '') || (isString(method.name) && method.name !== '')))
		throw new ArgumentsError('defineMethod(object, method, method_name): method_name or method.name must be non-empty string');
	var name = (isString(method_name) && method_name !== '') ? method_name : method.name;
	return Object.defineProperty(object, name, {
		configurable: false,
		enumerable: false,
		writable: false,
		value: method
	});
}

/**
 * Добавляет объекту указанное свойство
 * @param {Object} object - объект, которому будет добавлено свойство
 * @param {Object} property_definition - набор параметров свойства
 * @returns {Object}
 */
function defineProperty(object, property_definition) {
	if (!isObject(object))
		throw new TypeError('defineProperty(object, property_definition): Instance of Object expected as object');
	if (!isObject(property_definition))
		throw new TypeError('defineProperty(object, property_definition): Instance of Object expected as property_definition');
	var { name, enumerable, configurable, get, set, value, writable } = property_definition;
	if (!(isString(name) && name !== ''))
		throw new TypeError('defineProperty(object, property_definition): Instance of non-empty String expected as property_definition.name');
	enumerable = enumerable !== undefined ? !!enumerable : true;
	configurable = configurable !== undefined ? !!configurable : true;
	writable = writable !== undefined ? !!writable : true;
	if (isFunction(get) && isFunction(set)) {
		return Object.defineProperty(object, name, {
			get: get,
			set: set,
			configurable: configurable,
			enumerable: enumerable
		});
	} else if (isFunction(get)) {
		return Object.defineProperty(object, name, {
			get: get,
			configurable: configurable,
			enumerable: enumerable
		});
	} else {
		return Object.defineProperty(object, name, {
			value: value,
			configurable: configurable,
			enumerable: enumerable,
			writable: writable
		});
	}
}

function extractError(error) {
	try {
		return (error instanceof Error) ? `${error.message}\n${error.stack}` : `${error}`;
	} catch (unexpected) {
		console.error(`UNEXPECTED: ${unexpected.message}\n${unexpected.stack}`);
		throw unexpected;
	}
}

function escapeSql(value) {
	let result = undefined;
	if (value === undefined || value === null)
		result = 'NULL';
	else if (value === true || value === false)
		result = value.toString();
	else if (!isNaN(value) && value !== '')
		result = value.toString();
	else {
		let stringValue = (typeof value === 'string' || value instanceof String) ? value : value.toString();
		result = `'${stringValue.replace(/'/g, '\'\'')}'`;
	}
	// console.info(`escapeSql(${JSON.stringify(value)}) is ${JSON.stringify(result)}`);
	return result;
}


/**
 *
 * @param resolve
 * @type {Function} resolve
 * @param reject
 * @type {Function} reject
 * @param error
 * @param result
 */
function processResult(resolve, reject, error, result) {
	if (!isFunction(resolve)) {
		throw new TypeError('processResult(resolve, reject, error, result): Instance of Function expected as resolve');
	}
	if (!isFunction(reject)) {
		throw new TypeError('processResult(resolve, reject, error, result): Instance of Function expected as reject');
	}
	if (error) {
		reject(error);
	} else {
		resolve(result);
	}
}

/**
 *
 * @param func
 * @type {Function} func
 * @param resolve
 * @type {Function} resolve
 * @param reject
 * @type {Function} reject
 * @returns {*}
 */
function promiseExecutor(func, resolve, reject) {
	return func(processResult.bind(null, resolve, reject));
}

/**
 *
 * @param func
 * @type {Function} func
 * @returns {Promise.<*>}
 */
function promisify(func) {
	if (!isFunction(func)) {
		return Promise.reject(new TypeError('promisify(func): Instance of Function expected as func'));
	}
	try {
		return new Promise(promiseExecutor.bind(null, func));
	} catch (exception) {
		return Promise.reject(exception);
	}
}

module.exports = {
	defineMethod,
	defineProperty,
	getClassName,
	isArray,
	isBoolean,
	isFunction,
	isNumber,
	isInteger,
	isPositiveInteger,
	isPositiveNumber,
	isObject,
	isString,
	extend,
	extractError,
	escapeSql,
	promisify,
};

if (!isFunction(Object.values)) {
	Object.values = function values(object) {
		return Object.keys(object).map((key) => object[key]);
	};
}

if (!isFunction(Object.zip)) {
	Object.zip = function zip(object) {
		return Object.keys(object).map((key) => [key, object[key]]);
	};
}

if (!isFunction(Object.collect)) {
	Object.collect = function collect(object) {
		return Object.keys(object).map((key) => {
			return {
				key: key,
				value: object[key]
			};
		});
	};
}
