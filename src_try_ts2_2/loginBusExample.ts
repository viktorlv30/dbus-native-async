/// <reference path="../ts-interfaces/index.d.ts" />
/// <reference path="../ts-interfaces/utils.d.ts" />
/// <reference path="../node_modules/@types/node/index.d.ts"/>

const dbus = require('../index');
const systemBus = dbus.systemBus();
const sessionBus = dbus.sessionBus();

try {
    (async () => {
        console.log('START');
        let login1: any = undefined;
        //objname //path //name
        login1 = await systemBus.getInterface(
            'org.freedesktop.login1',
            '/org/freedesktop/login1',
            'org.freedesktop.login1.Manager');

        if (login1) {
            console.log('Manager ready');

            // console.log(`Login1 manager - \n`, login1);
            // for (let element in login1) {
            //     console.log(`${element} - ${login1[element]}`);
            //     console.log(`\n`);
            // };
            console.log('We can start to work!!! >>> ');
            let sesPIDId = undefined;

            sesPIDId = await login1.HandlePowerKey;
            console.log(`Login1 manager - sesPIDId - `, sesPIDId);

            // console.log(`login1.HandlePowerKey - `, await login1.HandlePowerKey);
            // sesPIDId = login1.HandlePowerKey();

            let resolve1: any = undefined;
            // let resolve2: any = undefined;
            resolve1 = await systemBus.getInterface(
                'org.freedesktop.resolve1',
                '/org/freedesktop/resolve1',
                'org.freedesktop.resolve1.Manager');
            let dns: object[] = await resolve1.DNS;
            dns.forEach(async (element: any[]) => {
                console.dir(element);
                try {
                    let [index, family, address] = element;
                    let name: object[] = await resolve1.ResolveAddress(index, family, address.data, 1);
                    console.dir(name);
                } catch (error) {
                    console.warn(`resolve1.ResolveAddress(ifindex, family, address, flags): ${error.message}`);
                }
            });

            // dns.forEach(async (element) => {
            //     // console.log(`element - `, element);
            //     console.log(`show dns - ${element[2].data}`);

            //     let address: object[] = await resolve2.method.ResolveAddress(
            //         element[0],
            //         element[1],
            //         element[2].data,
            //         1);

            //     console.log(`Host name - `, address);
            // });
        } else {
            throw new Error(`Manager isn't created`);
        }
        login1.on('SessionNew', function (sessionId, sessionPath) {
            console.log(`[NEW][SESSION][${sessionId}]: ${sessionPath}`);
            (async () => {
                try {
                    let resolve1: any = undefined;
                    resolve1 = await systemBus.getInterface(
                        'org.freedesktop.resolve1',
                        '/org/freedesktop/resolve1',
                        'org.freedesktop.resolve1.Manager');

                    // console.log(`Show resolve manager - `, resolve1);
                    let dns: object[] = await resolve1.DNS;
                    dns.forEach(async (element) => {
                        console.log(`element - `, element);
                        // console.log(`show dns - ${element[2].data}`);
                        // let address: object[] = await promisify(resolve1.ResolveAddress.bind(
                        //     resolve1,
                        //     element[0],
                        //     element[1],
                        //     element[2].data,
                        //     1));
                        // console.log(`Host name - `, address[0][1]);
                    });
                } catch (exception) {
                    console.error(`NEW session error - ${exception}`);
                }
            })();
        });
        login1.on('SessionRemoved', function (sessionId, sessionPath) {
            console.log(`[REMOVE][SESSION][${sessionId}]: ${sessionPath}`);
            (async () => {
                try {
                    let brightnessDisplayManager: any = undefined;
                    brightnessDisplayManager = await sessionBus.getInterface(
                        'org.freedesktop.PowerManagement',
                        '/org/kde/Solid/PowerManagement/Actions/BrightnessControl',
                        'org.kde.Solid.PowerManagement.Actions.BrightnessControl');

                    console.log(`brightnessDisplayManager - description`);
                    console.dir(brightnessDisplayManager);
                    let currentBrightness: number = await brightnessDisplayManager.brightness();
                    console.log(`end session currentBrightness - `, currentBrightness);
                    let brightnessMax: number = await brightnessDisplayManager.brightnessMax();
                    console.log(`end session brightnessMax - `, brightnessMax);
                    let newBrightness: number = (currentBrightness - 10) < 1000 ? brightnessMax : currentBrightness - 50;
                    // console.log(`end session newBrightness - `, newBrightness);
                    brightnessDisplayManager.setBrightness(newBrightness);
                } catch (exception) {
                    console.error(`end session brightnessDisplayManager REMOVE error - ${exception}`);
                }
            })();
        });
    })()
} catch (error) {
    console.error(`[ERROR] ${error}`);
}