export class Student {
    fullName: string;
    age: number;
    constructor(public firstName, public middleInitial, public lastName, public vozrast) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
        this.age = vozrast;
    }

    greeter(): string {
        return "Hello, " + this.fullName + ", he is " + this.age + " years old!";

    }
}

class Person implements IPerson {

    constructor(public firstName: string, public lastName: string, public age: number) {
    }

    g(): void {
        console.log(this.firstName + " " + this.lastName + " " + this.age);
    }

}

interface IPerson {
    firstName: string;
    lastName: string;
    age: number;
    g(): void;
}

let ivan: IPerson = new Person('Ivan', 'Petrovicch', 20);
ivan.g();

var user = new Student("Jane", "M.", "User", 25);

console.log(user.greeter());

// export {Student};
