/// <reference path="../ts-interfaces/utils.d.ts" />
const { promisify } = require('../utils/utils');

export class IntrospectAsync {
    private method: object;
    constructor(iface: object) {
        if (!iface) {
            throw new TypeError(`Constructor needs dbus native interface object!`);
        }
        this.method = iface;
        this.convertAllMethodsToAsync();
    }

    public toString(): void {
        console.log(`Class IntrospectAsync:`);
        console.log(`all methods of interface: \n`, this.method);

        console.log(`Class properties:`);
        for (const key of Object.keys(this.method)) {
            const val = this.method[key];
            console.log(`val - `, val);
            // use val
        }
    }

    public showMethodsName(): void {
        for (let name in this.method) {
            console.log(`     Method: `, name);
        }
    }

    public showFunctionsInMethod(): void {
        for (let name in this.method) {
            console.log(` >>>>>>>>>>  Method: ${name} = ${this.method[name]}`);
        }
    }

    private convertAllMethodsToAsync(): void {
        for (let methodName in this.method) {
            if (methodName == 'on') {
                // console.log(`Method 'on' the same`);
                continue;
            }
            if (methodName == 'addListener') {
                // console.log(`Method 'addListener' the same`);
                continue;
            }
            if (methodName == 'removeListener') {
                // console.log(`Method 'removeListener' the same`);
                continue;
            }
            let that = this.method;
            let oldMethod = that[methodName];
            this.method[methodName] = (async function () {
                return await promisify(oldMethod.bind(that, ...arguments));
            });
        }
    }

}

