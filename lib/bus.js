'use strict';

let EventEmitter = require('events').EventEmitter;
let constants = require('./constants');
let stdDbusIfaces = require('./stdifaces');
let { promisify } = require('../utils/utils');

class DBusObject {
    constructor(name, service) {
        // console.log(`call - DBusObject - ${name}`);
        this.name = name;
        this.service = service;
    }

    as(name) {
        // console.log(`call - DBusObject.as() - ${name}`);
        return this.proxy[name];
    };;
}

class DBusService {
    constructor(name, bus) {
        this.name = name;
        this.bus = bus;
    }

    async getObject(name) {
        // console.log(`call - DBusService.getObject() - `, name);
        var obj = new DBusObject(name, this);
        var introspect = require('./introspect.js');
        let ifaces = await promisify(introspect.bind(null, obj));
        obj.proxy = ifaces;
        return obj;
    };

    async getInterface(objName, ifaceName) {
        // console.log(`-- call - DBusService.getInterface() - `, JSON.stringify(arguments));
        let result = await this.getObject(objName);
        result = result.as(ifaceName);
        return result;
    };
}

module.exports = function bus(conn, opts) {
    if (!(this instanceof bus)) {
        return new bus(conn);
    }
    if (!opts) {
        opts = {};
    }

    var self = this;
    this.connection = conn;
    this.serial = 1;
    this.cookies = {}; // TODO: rename to methodReturnHandlers
    this.methodCallHandlers = {};
    this.signals = new EventEmitter();
    this.exportedObjects = {};
    this.counter = 0;

    // fast access to tree formed from object paths names
    // this.exportedObjectsTree = { root: null, children:  };

    this.invoke = function invoke(msg, callback) {
        let result = undefined;
        let exception = undefined;
        if (!msg.type) {
            msg.type = constants.messageType.methodCall;
        }
        msg.serial = self.serial;
        self.serial++;
        this.cookies[msg.serial] = callback;
        self.connection.message(msg);
    };

    this.invokeDbus = async function invokeDbus(msg, callback) {
        if (!msg.path)
            msg.path = '/org/freedesktop/DBus';
        if (!msg.destination)
            msg.destination = 'org.freedesktop.DBus';
        if (!msg['interface'])
            msg['interface'] = 'org.freedesktop.DBus';

        let result;
        let error;
        try {
            result = await promisify(self.invoke.bind(self, msg));

        } catch (exception) {
            error = exception;
        }

        if (typeof callback === 'function') {
            try {
                process.nextTick(callback.bind(undefined, error, result));
            } catch (exception) {
                error = exception;
            }
        }
        if (error !== undefined) {
            throw error;
        }

        return result;
    };

    this.mangle = function mangle(path, iface, member) {
        // console.log(`-- this.mangle()`/* arguments - ${JSON.stringify(arguments)}`*/);
        var obj = {};
        if (typeof path === 'object') // handle one argumant case mangle(msg)
        {
            obj.path = path.path;
            obj['interface'] = path['interface'];
            obj.member = path.member;
        } else {
            obj.path = path;
            obj['interface'] = iface;
            obj.member = member;
        }
        // console.log(`-- this.mangle() result - `, JSON.stringify(obj));
        return JSON.stringify(obj);
    };

    this.sendSignal = function sendSignal(path, iface, name, signature, args) {
        // console.log(`-- this.sendSignal() arguments - ${JSON.stringify(arguments)}`);
        var signalMsg = {
            type: constants.messageType.signal,
            serial: self.serial,
            'interface': iface,
            path: path,
            member: name
        };
        if (signature) {
            signalMsg.signature = signature;
            signalMsg.body = args;
        }
        self.connection.message(signalMsg);
    };

    this.sendError = function sendError(msg, errorName, errorText) {
        // console.log(`-- this.sendError() arguments - ${JSON.stringify(arguments)}`);
        if (/^[a-zA-Z0-9]+$/.exec(errorName) !== null) {
            errorName = `org.freedesktop.DBus.Error.${errorName}`;
        } else if (/^[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*$/.exec(errorName) === null) {
            console.error(`INVALID DBUS ERROR NAME: ${JSON.stringify(errorName)}`);
            process.exit(255);
        }
        var reply = {
            type: constants.messageType.error,
            replySerial: msg.serial,
            destination: msg.sender,
            errorName: errorName,
            signature: 's',
            body: [errorText]
        };
        //console.log('SEND ERROR', reply);
        this.connection.message(reply);
    };

    this.sendReply = function sendReply(msg, signature, body) {
        // console.log(`[${++this.counter}] call - this.sendReply`);
        var reply = {
            type: constants.messageType.methodReturn,
            replySerial: msg.serial,
            destination: msg.sender,
            signature: signature,
            body: body
        };
        this.connection.message(msg);
    };

    // route reply/error
    this.connection.on('message', async function (msg) {
        var msg = JSON.parse(JSON.stringify(msg));
        var handler;
        if (msg.type == constants.messageType.methodReturn || msg.type == constants.messageType.error) {
            // console.log(`call - self.cookies[${msg.replySerial}]`);
            handler = self.cookies[msg.replySerial];
            // console.log(`msg.replySerial - `, msg.replySerial);
            // console.log(`handler - `, handler);
            if (msg.type == constants.messageType.methodReturn && msg.body)
                msg.body.unshift(null); // first argument - no errors, null
            if (handler) {
                delete self.cookies[msg.replySerial];
                var props = {
                    connection: self.connection,
                    bus: self,
                    message: msg,
                    signature: msg.signature
                };
                if (msg.type == constants.messageType.methodReturn)
                    handler.apply(props, msg.body); // body as array of arguments
                else
                    handler.call(props, msg.body);  // body as first argument
            }
        } else if (msg.type == constants.messageType.signal) {
            self.signals.emit(self.mangle(msg), msg.body, msg.signature);
        } else { // methodCall

            // exported interfaces handlers
            var obj, iface, impl;
            if (obj = self.exportedObjects[msg.path]) {

                if (stdDbusIfaces(msg, self))
                    return;
                try {
                    let iface = obj[msg.interface];
                    if (iface) {
                        let [declaration, implementation] = iface;
                        let method = implementation[msg.member];
                        if (typeof method !== 'function') {
                            let error = new Error(`Method ${msg.interface}.${msg.member} of ${msg.path} is not implemented`);
                            error.name = 'org.freedesktop.DBus.Error.MethodNotImplemented';
                            throw error;
                        }
                        let result = method.apply(implementation, msg.body);
                        if (result instanceof Promise) {
                            result = await result;
                        }
                        // console.dir(result);
                        let reply = {
                            type: constants.messageType.methodReturn,
                            destination: msg.sender,
                            replySerial: msg.serial
                        };
                        if (result !== undefined) {
                            reply.signature = declaration.methods[msg.member][1];
                            reply.body = [result];
                        }
                        self.connection.message(reply);
                    } else {
                        let error = new Error(`Interface ${msg.interface} of ${msg.path} is not implemented`);
                        error.name = 'org.freedesktop.DBus.Error.InterfaceNotImplemented';
                        throw error;
                    }
                } catch (error) {
                    if (error instanceof Error) {
                        // console.info(`${error.name}: ${error.message}\n${error.stack}`);
                        self.sendError(msg, error.name, error.message);
                    } else {
                        // console.info(`Unknown Error: ${error}`);
                        self.sendError(msg, 'org.freedesktop.DBus.Error', `${error}`);
                    }
                }
            } else {
                handler = self.methodCallHandlers[self.mangle(msg)];
                if (handler) {
                    var result;
                    try {
                        result = handler[0].apply(null, msg.body);
                    } catch (e) {
                        console.error("Caught exception while trying to execute handler: ", e);
                        self.sendError(e.message, e.description);
                        return;
                    }
                    var reply = {
                        type: constants.messageType.methodReturn,
                        destination: msg.sender,
                        replySerial: msg.serial
                        //, sender: self.name
                    };
                    if (result) {
                        reply.signature = handler[1];
                        reply.body = result;
                    }
                    self.connection.message(reply);
                } else {
                    self.sendError(msg, 'org.freedesktop.DBus.Error.UnknownService', 'Uh oh oh');
                }
            }
            // setMethodCall handlers
        }
    });

    this.setMethodCallHandler = function setMethodCallHandler(objectPath, iface, member, handler) {
        // console.log(`[${++this.counter}] call - this.setMethodCallHandler`);
        var key = self.mangle(objectPath, iface, member);
        self.methodCallHandlers[key] = handler;
    };

    this.exportInterface = function exportInterface(obj, path, iface) {
        // console.log(`[${++this.counter}] call - this.exportInterface`);
        var entry;
        if (!self.exportedObjects[path])
            entry = self.exportedObjects[path] = {};
        else
            entry = self.exportedObjects[path];
        entry[iface.name] = [iface, obj];
        // monkey-patch obj.emit()
        if (typeof obj.emit === 'function') {
            var oldEmit = obj.emit;
            obj.emit = function () {
                var args = Array.prototype.slice.apply(arguments);
                var signalName = args[0];
                if (!signalName)
                    throw new Error('Trying to emit undefined signal');

                //send signal to bus
                var signal;
                if (iface.signals && iface.signals[signalName]) {
                    signal = iface.signals[signalName];
                    //console.log(iface.signals, iface.signals[signalName]);
                    var signalMsg = {
                        type: constants.messageType.signal,
                        serial: self.serial,
                        'interface': iface.name,
                        path: path,
                        member: signalName
                    };
                    if (signal[0]) {
                        signalMsg.signature = signal[0];
                        signalMsg.body = args.slice(1);
                    }
                    self.connection.message(signalMsg);
                    self.serial++;
                }
                // note that local emit is likely to be called before signal arrives
                // to remote subscriber
                oldEmit.apply(obj, args);
            };
        }
        // TODO: emit ObjectManager's InterfaceAdded
    };

    // register name
    if (opts.direct !== true) {
        (async () => {
            self.name = await promisify(this.invokeDbus.bind(this, { member: 'Hello' }));
            // self.name = await this.invokeDbus({ member: 'Hello' });
            // console.log(`self.name during start - `, self.name);
        })();
    } else {
        self.name = null;
        // console.log(`name - `, name);
    }

    this.getService = function getService(name) {
        // console.log(`-- this.invoke() arguments - ${JSON.stringify(arguments)}`);
        return new DBusService(name, this);
    };

    this.getObject = async function getObject(path, name) {
        // console.log(`-- this.invoke() arguments - ${JSON.stringify(arguments)}`);
        var service = this.getService(path);
        return await service.getObject(name);
    };

    this.getInterface = async function getInterface(path, objname, name) {
        // console.log(`-- this.invoke() arguments - ${JSON.stringify(arguments)}`);
        let result = await this.getObject(path, objname);
        result = result.as(name);
        return result;
    };

    this.convertToAsync = async function (obj, callback) {
        let result;
        let error;
        try {
            result = await this.invokeDbus(obj);
        } catch (exception) {
            error = exception;
        }

        if (typeof callback === 'function') {
            try {
                process.nextTick(callback.bind(undefined, error, result));
            } catch (exception) {
                error = exception;
            }
        }
        if (error !== undefined) {
            throw error;
        }

        return result;
    }

    // bus meta functions
    this.addMatch = async function addMatch(match, callback) {
        //TODO - think about is need next check for self.name 
        // I have commented it 05.07.2017 ('Bitretail project Temabit')

        /*
        if (!self.name) {
             console.log(`-- this.addMatch() arguments - ${JSON.stringify(arguments)}`);
             throw new Error(`bus.addMatch() self.name isn't defined`);
         }
        */

        return await this.convertToAsync(
            { 'member': 'AddMatch', signature: 's', body: [match] },
            callback
        )
    };

    this.removeMatch = async function removeMatch(match, callback) {
        if (!self.name)
            throw new Error(`bus.removeMatch() self.name isn't defined`);
        return await this.convertToAsync(
            { 'member': 'RemoveMatch', signature: 's', body: [match] },
            callback
        );
    };

    this.getId = async function getId(callback) {
        return await this.convertToAsync(
            { 'member': 'GetId' },
            callback
        );
    };

    this.requestName = async function (name, flags, callback) {
        let result;
        let error;

        try {
            let recName = await promisify(this.invokeDbus.bind(this, { 'member': 'RequestName', signature: 'su', body: [name, flags] }));
        } catch (exception) {
            error = exception;
        }

        if (typeof callback === 'function') {
            try {
                process.nextTick(callback.bind(undefined, error, result));
            } catch (exception) {
                error = exception;
            }
        }
        if (error !== undefined) {
            throw error;
        }
        return result;
    };

    this.releaseName = async function (name, callback) {
        return await this.convertToAsync(
            { 'member': 'ReleaseName', signature: 's', body: [name] },
            callback
        );
    };

    this.listNames = async function listNames(callback) {
        return await this.convertToAsync(
            { 'member': 'ListNames' },
            callback
        );
    };

    this.listActivatableNames = async function listActivatableNames(name, callback) {
        return await this.convertToAsync(
            { 'member': 'ListActivatableNames', signature: 's', body: [name] },
            callback
        );
    };

    this.updateActivationEnvironment = async function updateActivationEnvironment(env, callback) {
        return await this.convertToAsync(
            { 'member': 'UpdateActivationEnvironment', signature: 'a{ss}', body: [env] },
            callback
        );
    };

    this.startServiceByName = async function startServiceByName(name, flags, callback) {
        return await this.convertToAsync(
            { 'member': 'StartServiceByName', signature: 'su', body: [name, flags] },
            callback
        );
    };

    this.getConnectionUnixUser = async function getConnectionUnixUser(name, callback) {
        return await this.convertToAsync(
            { 'member': 'GetConnectionUnixUser', signature: 's', body: [name] },
            callback
        );
    };

    this.getConnectionUnixProcessId = async function getConnectionUnixProcessId(name, callback) {
        return await this.convertToAsync(
            { 'member': 'GetConnectionUnixProcessID', signature: 's', body: [name] },
            callback
        );
    };

    this.getNameOwner = async function getNameOwner(name, callback) {
        return await this.convertToAsync(
            { 'member': 'GetNameOwner', signature: 's', body: [name] },
            callback
        );
    };

    this.nameHasOwner = async function nameHasOwner(name, callback) {
        return await this.convertToAsync(
            { 'member': 'GetNameOwner', signature: 's', body: [name] },
            callback
        );
    };
};
