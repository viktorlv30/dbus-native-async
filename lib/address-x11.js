// read dbus adress from window selection

var x11 = require('x11');
var fs = require('fs');
var os = require('os');
var { promisify } = require('../utils/utils');

async function getDbusAddress() {
    // read machine uuid    
    try{
        let uuid = await promisify(fs.readFile.bind(null, '/var/lib/dbus/machine-id', 'ascii'));
        var hostname = os.hostname().split('-')[0];

        let display = await promisify(x11.createClient);
        var X = display.client;
        var selectionName = `_DBUS_SESSION_BUS_SELECTION_${hostName}_${uuid.trim()}`;
        var id = await promisify(X.InternAtom.bind(X, false, selectionName));
        var win = await promisify(X.GetSelectionOwner.bind(X, id));
        var propId = await promisify(X.InternAtom.bind(X, false, '_DBUS_SESSION_BUS_ADDRESS'));
        win = display.screen[0].root;
        var val = await promisify(X.GetProperty.bind(X, 0, win, propId, 0, 0, 10000000));
        return val.data.toString();

    } catch(exception){
        return exception;
    }

//     fs.readFile('/var/lib/dbus/machine-id', 'ascii', function(err, uuid) {
//         if (err) return callback(err);
//         var hostname = os.hostname().split('-')[0];
//         x11.createClient(function(err, display) {
//             var X = display.client;
//             var selectionName = '_DBUS_SESSION_BUS_SELECTION_' + hostname + '_' + uuid.trim();
//             X.InternAtom(false, selectionName, function(err, id) {
//                 if (err) return callback(err);
//                 X.GetSelectionOwner(id, function(err, win) {
//                     if (err) return callback(err);
//                     X.InternAtom(false, '_DBUS_SESSION_BUS_ADDRESS', function(err, propId) {
//                         if (err) return callback(err);
//                         win = display.screen[0].root;
//                         X.GetProperty(0, win, propId, 0, 0, 10000000, function(err, val) {
//                             if (err) return callback(err);
//                             callback(null, val.data.toString());
//                         });
//                    });
//                });
//            });
//        });
//    });
}

module.exports = getDbusAddress;
//getDbusAddress(console.log);
