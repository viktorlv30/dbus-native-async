const dbus = require('../index.js');
const systemBus = dbus.systemBus();
const sessionBus = dbus.sessionBus();
const { promisify } = require('../utils/utils');

let argv = process.argv;

try {
	(async () => {
		console.log('START');
		let login1 = undefined;
		if (argv[2] && argv[2] === '-m') {
			// console.log(`login1 - Start 'my custom' implementation of dbus with '-m' argument`);
			//objname //path //name
			login1 = await systemBus.getInterface(
				'org.freedesktop.login1',
				'/org/freedesktop/login1',
				'org.freedesktop.login1.Manager');
		}
		if (argv[2] && argv[2] === '-n') {
			// console.log(`login1 - Start 'native' implementation of dbus with '-n' argument`);
			//objname //path //name
			login1 = await promisify(systemBus.getInterface.bind(
				systemBus,
				'org.freedesktop.login1',
				'/org/freedesktop/login1',
				'org.freedesktop.login1.Manager'));
		}
		if (login1) {
			console.log('Manager ready');
			// console.dir(login1);
			console.log('We can start to work!!! >>> ');
		} else {
			throw new Error(`Manager isn't created`);
		}
		login1.on('SessionNew', function (sessionId, sessionPath) {
			console.log(`[NEW][SESSION][${sessionId}]: ${sessionPath}`);
			(async () => {
				try {
					let resolve1 = undefined;
					if (argv[2] && argv[2] === '-m') {
						// console.log(`SessionNew Start 'my custom' implementation of dbus with '-m' argument`);
						resolve1 = await systemBus.getInterface(
							'org.freedesktop.resolve1',
							'/org/freedesktop/resolve1',
							'org.freedesktop.resolve1.Manager');
					}
					if (argv[2] && argv[2] === '-n') {
						// console.log(`SessionNew Start 'native' implementation of dbus with '-n' argument`);
						resolve1 = await promisify(systemBus.getInterface.bind(
							systemBus,
							'org.freedesktop.resolve1',
							'/org/freedesktop/resolve1',
							'org.freedesktop.resolve1.Manager'));
					}
					// console.log(`Show resolve manager - `, resolve1);
					let dns = await promisify(resolve1.DNS);
					dns.forEach(async (element) => {
						// console.log(`element - `, element);
						console.log(`show dns - ${element[2].data}`);
						let address = await promisify(resolve1.ResolveAddress.bind(
							resolve1,
							element[0],
							element[1],
							element[2].data,
							1));
						console.log(`Host name - `, address[0][1]);
					});
				} catch (exception) {
					console.error(`NEW session error - ${exception}`);
				}
			})();
		});
		login1.on('SessionRemoved', function (sessionId, sessionPath) {
			console.log(`[REMOVE][SESSION][${sessionId}]: ${sessionPath}`);
			(async () => {
				try {
					let brightnessDisplayManager = undefined;
					if (argv[2] && argv[2] === '-m') {
						// console.log(`SessionRemoved Start 'my custom' implementation of dbus with '-m' argument`);
						brightnessDisplayManager = await sessionBus.getInterface(
							'org.freedesktop.PowerManagement',
							'/org/kde/Solid/PowerManagement/Actions/BrightnessControl',
							'org.kde.Solid.PowerManagement.Actions.BrightnessControl');
					}
					if (argv[2] && argv[2] === '-n') {
						// console.log(`SessionRemoved Start 'native' implementation of dbus with '-n' argument`);
						brightnessDisplayManager = await promisify(sessionBus.getInterface.bind(
							sessionBus,
							'org.freedesktop.PowerManagement',
							'/org/kde/Solid/PowerManagement/Actions/BrightnessControl',
							'org.kde.Solid.PowerManagement.Actions.BrightnessControl'));
					}
					let currentBrightness = await promisify(brightnessDisplayManager.brightness);
					// console.log(`end session currentBrightness - `, currentBrightness);
					let brightnessMax = await promisify(brightnessDisplayManager.brightnessMax);
					// console.log(`end session brightnessMax - `, brightnessMax);
					let newBrightness = (currentBrightness - 10) < 1000 ? brightnessMax : currentBrightness - 10;
					console.log(`end session newBrightness - `, newBrightness);
					brightnessDisplayManager.setBrightness(newBrightness);
				} catch (exception) {
					console.error(`end session brightnessDisplayManager REMOVE error - ${exception}`);
				}
			})();
		});
	})()
} catch (error) {
	console.error(`[ERROR] ${error}`);
	return;
}