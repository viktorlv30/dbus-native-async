node dbus-native-async
===========
D-bus protocol client and server for node.js

Installation
------------

```shell
npm install dbus-native-async
```
or

```shell
git clone https://viktorlv30@bitbucket.org/viktorlv30/dbus-native-async.git # clone the repo
cd node-dbus-async
npm install # install dependencies
sudo cp examples/build_try_ts2_2
node loginBusExample.js # if you want to test examples/loginBusExample.js
after call another console as Alt+F2 and login into one more time and return to first console - you should see the result
```

Usage
------

Short example using desktop notifications service

```ts
/// <reference path="../ts-interfaces/index.d.ts" />
/// <reference path="../ts-interfaces/utils.d.ts" />
/// <reference path="../node_modules/@types/node/index.d.ts"/>

const dbus = require('../index');
const systemBus = dbus.systemBus();
const sessionBus = dbus.sessionBus();

try {
    (async () => {
        console.log('START');
        let login1: any = undefined;
        //objname //path //name
        login1 = await systemBus.getInterface(
            'org.freedesktop.login1',
            '/org/freedesktop/login1',
            'org.freedesktop.login1.Manager');

        if (login1) {
            console.log('Manager ready');
            console.log('We can start to work!!! >>> ');
            let sesPIDId = undefined;

            sesPIDId = await login1.HandlePowerKey;
            console.log(`Login1 manager - sesPIDId - `, sesPIDId);
            let resolve1: any = undefined;
            resolve1 = await systemBus.getInterface(
                'org.freedesktop.resolve1',
                '/org/freedesktop/resolve1',
                'org.freedesktop.resolve1.Manager');
            let dns: object[] = await resolve1.DNS;
            dns.forEach(async (element: any[]) => {
                console.dir(element);
                try {
                    let [index, family, address] = element;
                    let name: object[] = await resolve1.ResolveAddress(index, family, address.data, 1);
                    console.dir(name);
                } catch (error) {
                    console.warn(`resolve1.ResolveAddress(ifindex, family, address, flags): ${error.message}`);
                }
            });
        } else {
            throw new Error(`Manager isn't created`);
        }
        login1.on('SessionNew', function (sessionId, sessionPath) {
            console.log(`[NEW][SESSION][${sessionId}]: ${sessionPath}`);
            (async () => {
                try {
                    let resolve1: any = undefined;
                    resolve1 = await systemBus.getInterface(
                        'org.freedesktop.resolve1',
                        '/org/freedesktop/resolve1',
                        'org.freedesktop.resolve1.Manager');
                    let dns: object[] = await resolve1.DNS;
                    dns.forEach(async (element) => {
                        console.log(`element - `, element);
                    });
                } catch (exception) {
                    console.error(`NEW session error - ${exception}`);
                }
            })();
        });
        login1.on('SessionRemoved', function (sessionId, sessionPath) {
            console.log(`[REMOVE][SESSION][${sessionId}]: ${sessionPath}`);
            (async () => {
                try {
                    let brightnessDisplayManager: any = undefined;
                    brightnessDisplayManager = await sessionBus.getInterface(
                        'org.freedesktop.PowerManagement',
                        '/org/kde/Solid/PowerManagement/Actions/BrightnessControl',
                        'org.kde.Solid.PowerManagement.Actions.BrightnessControl');

                    console.log(`brightnessDisplayManager - description`);
                    console.dir(brightnessDisplayManager);
                    let currentBrightness: number = await brightnessDisplayManager.brightness();
                    console.log(`end session currentBrightness - `, currentBrightness);
                    let brightnessMax: number = await brightnessDisplayManager.brightnessMax();
                    console.log(`end session brightnessMax - `, brightnessMax);
                    let newBrightness: number = (currentBrightness - 10) < 1000 ? brightnessMax : currentBrightness - 50;
                    // console.log(`end session newBrightness - `, newBrightness);
                    brightnessDisplayManager.setBrightness(newBrightness);
                } catch (exception) {
                    console.error(`end session brightnessDisplayManager REMOVE error - ${exception}`);
                }
            })();
        });
    })()
} catch (error) {
    console.error(`[ERROR] ${error}`);
}
```

API
---

### Low level messaging: bus connection

`connection = dbus.createClient(options)`

options:
   - socket - unix socket path
   - port - TCP port
   - host - TCP host
   - busAddress - encoded bus address. Default is `DBUS_SESSION_BUS_ADDRESS` environment variable. See http://dbus.freedesktop.org/doc/dbus-specification.html#addresses
   - ( TODO: add/document option to use adress from X11 session )

connection has only one method, `message(msg)`

message fields:
   - type - methodCall, methodReturn, error or signal
   - path - object path
   - interface
   - destination
   - sender
   - member
   - serial
   - signature
   - body
   - errorName
   - replySerial

connection signals:
   - connect - emitted after successful authentication
   - message
   - error

example:

```js
var dbus = require('dbus-native-async');
var conn = dbus.createConnection();
conn.message({
    path:'/org/freedesktop/DBus',
    destination: 'org.freedesktop.DBus',
    'interface': 'org.freedesktop.DBus',
    member: 'Hello',
    type: dbus.messageType.methodCall
});
conn.on('message', function(msg) { console.log(msg); });
```


### Links
   - https://github.com/sidorares/node-dbus - original project
   - http://cgit.freedesktop.org/dbus - freedesktop reference C library
