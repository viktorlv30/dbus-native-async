var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const dbus = require('../index');
const systemBus = dbus.systemBus();
const sessionBus = dbus.sessionBus();
try {
    (() => __awaiter(this, void 0, void 0, function* () {
        console.log('START');
        let login1 = undefined;
        login1 = yield systemBus.getInterface('org.freedesktop.login1', '/org/freedesktop/login1', 'org.freedesktop.login1.Manager');
        if (login1) {
            console.log('Manager ready');
            console.log('We can start to work!!! >>> ');
            let sesPIDId = undefined;
            sesPIDId = yield login1.HandlePowerKey;
            console.log(`Login1 manager - sesPIDId - `, sesPIDId);
            let resolve1 = undefined;
            resolve1 = yield systemBus.getInterface('org.freedesktop.resolve1', '/org/freedesktop/resolve1', 'org.freedesktop.resolve1.Manager');
            let dns = yield resolve1.DNS;
            dns.forEach((element) => __awaiter(this, void 0, void 0, function* () {
                console.dir(element);
                try {
                    let [index, family, address] = element;
                    let name = yield resolve1.ResolveAddress(index, family, address.data, 1);
                    console.dir(name);
                }
                catch (error) {
                    console.warn(`resolve1.ResolveAddress(ifindex, family, address, flags): ${error.message}`);
                }
            }));
        }
        else {
            throw new Error(`Manager isn't created`);
        }
        login1.on('SessionNew', function (sessionId, sessionPath) {
            console.log(`[NEW][SESSION][${sessionId}]: ${sessionPath}`);
            (() => __awaiter(this, void 0, void 0, function* () {
                try {
                    let resolve1 = undefined;
                    resolve1 = yield systemBus.getInterface('org.freedesktop.resolve1', '/org/freedesktop/resolve1', 'org.freedesktop.resolve1.Manager');
                    let dns = yield resolve1.DNS;
                    dns.forEach((element) => __awaiter(this, void 0, void 0, function* () {
                        console.log(`element - `, element);
                    }));
                }
                catch (exception) {
                    console.error(`NEW session error - ${exception}`);
                }
            }))();
        });
        login1.on('SessionRemoved', function (sessionId, sessionPath) {
            console.log(`[REMOVE][SESSION][${sessionId}]: ${sessionPath}`);
            (() => __awaiter(this, void 0, void 0, function* () {
                try {
                    let brightnessDisplayManager = undefined;
                    brightnessDisplayManager = yield sessionBus.getInterface('org.freedesktop.PowerManagement', '/org/kde/Solid/PowerManagement/Actions/BrightnessControl', 'org.kde.Solid.PowerManagement.Actions.BrightnessControl');
                    console.log(`brightnessDisplayManager - description`);
                    console.dir(brightnessDisplayManager);
                    let currentBrightness = yield brightnessDisplayManager.brightness();
                    console.log(`end session currentBrightness - `, currentBrightness);
                    let brightnessMax = yield brightnessDisplayManager.brightnessMax();
                    console.log(`end session brightnessMax - `, brightnessMax);
                    let newBrightness = (currentBrightness - 10) < 1000 ? brightnessMax : currentBrightness - 50;
                    brightnessDisplayManager.setBrightness(newBrightness);
                }
                catch (exception) {
                    console.error(`end session brightnessDisplayManager REMOVE error - ${exception}`);
                }
            }))();
        });
    }))();
}
catch (error) {
    console.error(`[ERROR] ${error}`);
}
//# sourceMappingURL=loginBusExample.js.map