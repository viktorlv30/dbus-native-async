"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Student {
    constructor(firstName, middleInitial, lastName, vozrast) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.vozrast = vozrast;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
        this.age = vozrast;
    }
    greeter() {
        return "Hello, " + this.fullName + ", he is " + this.age + " years old!";
    }
}
exports.Student = Student;
class Person {
    constructor(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    g() {
        console.log(this.firstName + " " + this.lastName + " " + this.age);
    }
}
let ivan = new Person('Ivan', 'Petrovicch', 20);
ivan.g();
var user = new Student("Jane", "M.", "User", 25);
console.log(user.greeter());
//# sourceMappingURL=_signature.js.map