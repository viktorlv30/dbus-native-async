/**
 * Helper module
 * Interface for typescript
 *
 * to include this interface you should use next string in your *.ts file as file's header
 * /// <reference path="../utils/utils.d.ts" />
 */
declare module "utils" {

    function isBoolean(value: any): boolean;

    function isString(value: any): boolean;

    function isNumber(value: any): boolean;

    function isInteger(value: any): boolean;

    function isPositiveNumber(value: any): boolean;

    function isPositiveInteger(value: any): boolean;

    function isFunction(value: any): boolean;

    function isArray(value: any): boolean;

    function isObject(value: any, BaseClass: any): boolean;

    function getClassName(value: any): string;

    function extend(object: object | object[], properties: object | object[]): object | object[];

    function defineMethod(object: object, method: (...parameters: any[]) => any, method_name?: string): object;

    function defineProperty(object: object, property_definition: object): object;

    function extractError(error: Error): string;

    function escapeSql(value: any): string;

    function processResult(resolve: (result: any) => any, reject: (error: Error) => void, error: Error, result: any): void;

    function promiseExecutor(func: (...params: any[]) => any, resolve: (result: any) => any, reject: (error: Error) => void): object;

    function promisify(func: (...params: any[]) => any): object;

}


// if (!isFunction(Object.values)) {
//     Object.values = function values(object) {
//         return Object.keys(object).map((key) => object[key]);
//     };
// }

// if (!isFunction(Object.zip)) {
//     Object.zip = function zip(object) {
//         return Object.keys(object).map((key) => [key, object[key]]);
//     };
// }

// if (!isFunction(Object.collect)) {
//     Object.collect = function collect(object) {
//         return Object.keys(object).map((key) => {
//             return {
//                 key: key,
//                 value: object[key]
//             };
//         });
//     };
// }
